package br.com.roadcard.teste.adapter.outbound.postgres.domains.handlers;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.roadcard.teste.adapter.outbound.postgres.domains.entity.Transportador;
import br.com.roadcard.teste.adapter.outbound.postgres.domains.repository.TransRepository;
import br.com.roadcard.teste.core.commons.TransportadorVO;
import br.com.roadcard.teste.core.port.outbound.TransportadorPortOutbound;

@Component
public class TransportadorAdapterOutboundHandler implements TransportadorPortOutbound {

	@Autowired
	private TransRepository repository;

	@Override
	public List<TransportadorVO> getAll() {
		List<TransportadorVO> transportadoresRet = new LinkedList<>();
		List<Transportador> transpEnt = repository.findAll();
		transpEnt.forEach(trans -> {
			TransportadorVO vo = Transportador.buildVOFromEntity(trans);
			transportadoresRet.add(vo);
		});

		return transportadoresRet;
	}

	@Override
	public TransportadorVO findById(Long id) {
		TransportadorVO transVO = null;
		Optional<Transportador> transpOpt = repository.findById(id);
		if (transpOpt.isPresent()) {
			Transportador transp = transpOpt.get();
			transVO = new TransportadorVO();

			transVO.setId(transp.getId());
			transVO.setAdimplenteAssociacao(transp.isAdimplenteAssociacao());
			transVO.setCapitalSocial(transp.getCapitalSocial());
			transVO.setCategoriaCnh(transp.getCategoriaCnh());
			transVO.setCodigoSituacaoRntrc(transp.getCodigoSituacaoRntrc());
			transVO.setCpfCnpj(transp.getCpfCnpj());
			transVO.setDataDeCadastro(transp.getDataDeCadastro());
			transVO.setDataDeNascimento(transp.getDataDeNascimento());
			transVO.setDescricaoSituacaoRntrc(transp.getDescricaoSituacaoRntrc());
			transVO.setEmailMovimentacaoFrota(transp.isEmailMovimentacaoFrota());
			transVO.setIdentidade(transp.getIdentidade());
			transVO.setInfoComplDescricao(transp.getInfoComplDescricao());
			transVO.setInscricaoEstadual(transp.getInscricaoEstadual());
			transVO.setInscricaoOcb(transp.getInscricaoOcb());
			transVO.setNomeCompleto(transp.getNomeCompleto());
			transVO.setNomeFantasia(transp.getNomeFantasia());
			transVO.setNomeMae(transp.getNomeMae());
			transVO.setNumeroCnh(transp.getNumeroCnh());
			transVO.setNumeroRegistroJunta(transp.getNumeroRegistroJunta());
			transVO.setNumeroRntrc(transp.getNumeroRntrc());
			transVO.setOrgaoEmissor(transp.getOrgaoEmissor());
			transVO.setSexo(transp.getSexo());
			transVO.setSituacaoRntrc(transp.getSituacaoRntrc());
			transVO.setTipoTransportador(transp.getTipoTransportador());
			transVO.setUf(transp.getUf());
			transVO.setValidadeRntrc(transp.getValidadeRntrc());
			transVO.setRowVersion(transp.getRowVersion());

		}

		return transVO;
	}

	@Override
	public TransportadorVO persist(TransportadorVO tr) {
		Transportador newTr = new Transportador();

		newTr.setAdimplenteAssociacao(tr.isAdimplenteAssociacao());
		newTr.setAdimplenteAssociacao(tr.isAdimplenteAssociacao());
		newTr.setCapitalSocial(tr.getCapitalSocial());
		newTr.setCategoriaCnh(tr.getCategoriaCnh());
		newTr.setCodigoSituacaoRntrc(tr.getCodigoSituacaoRntrc());
		newTr.setCpfCnpj(tr.getCpfCnpj());
		newTr.setDataDeCadastro(tr.getDataDeCadastro());
		newTr.setDataDeNascimento(tr.getDataDeNascimento());
		newTr.setDescricaoSituacaoRntrc(tr.getDescricaoSituacaoRntrc());
		newTr.setEmailMovimentacaoFrota(tr.isEmailMovimentacaoFrota());
		newTr.setIdentidade(tr.getIdentidade());
		newTr.setInfoComplDescricao(tr.getInfoComplDescricao());
		newTr.setInscricaoEstadual(tr.getInscricaoEstadual());
		newTr.setInscricaoOcb(tr.getInscricaoOcb());
		newTr.setNomeCompleto(tr.getNomeCompleto());
		newTr.setNomeFantasia(tr.getNomeFantasia());
		newTr.setNomeMae(tr.getNomeMae());
		newTr.setNumeroCnh(tr.getNumeroCnh());
		newTr.setNumeroRegistroJunta(tr.getNumeroRegistroJunta());
		newTr.setNumeroRntrc(tr.getNumeroRntrc());
		newTr.setOrgaoEmissor(tr.getOrgaoEmissor());
		newTr.setSexo(tr.getSexo());
		newTr.setSituacaoRntrc(tr.getSituacaoRntrc());
		newTr.setTipoTransportador(tr.getTipoTransportador());
		newTr.setUf(tr.getUf());
		newTr.setValidadeRntrc(tr.getValidadeRntrc());
		newTr.setRowVersion(tr.getRowVersion());

		Transportador retorno = repository.save(newTr);
		tr.setId(retorno.getId());

		// metodo save do repository retorno uma entity persistida(com Id), jogar em uma
		// variavel.
		// pegar tranportadorVO (tr) pegar o id da entity e setar no atributo id do tr
		// o metodo persist vai retornar o tr

		return tr;
	}

	@Override
	public TransportadorVO upDate(Long id, TransportadorVO tr) {
		TransportadorVO transVO = null;
		Optional<Transportador> transpOpt = repository.findById(id);
		if (transpOpt.isPresent()) {
			Transportador transp = transpOpt.get();
			Transportador newTr = new Transportador();

			newTr.setId(tr.getId());
			newTr.setAdimplenteAssociacao(tr.isAdimplenteAssociacao());
			newTr.setCapitalSocial(tr.getCapitalSocial());
			newTr.setCategoriaCnh(tr.getCategoriaCnh());
			newTr.setCodigoSituacaoRntrc(tr.getCodigoSituacaoRntrc());
			newTr.setCpfCnpj(tr.getCpfCnpj());
			newTr.setDataDeCadastro(tr.getDataDeCadastro());
			newTr.setDataDeNascimento(tr.getDataDeNascimento());
			newTr.setDescricaoSituacaoRntrc(tr.getDescricaoSituacaoRntrc());
			newTr.setEmailMovimentacaoFrota(tr.isEmailMovimentacaoFrota());
			newTr.setIdentidade(tr.getIdentidade());
			newTr.setInfoComplDescricao(tr.getInfoComplDescricao());
			newTr.setInscricaoEstadual(tr.getInscricaoEstadual());
			newTr.setInscricaoOcb(tr.getInscricaoOcb());
			newTr.setNomeCompleto(tr.getNomeCompleto());
			newTr.setNomeFantasia(tr.getNomeFantasia());
			newTr.setNomeMae(tr.getNomeMae());
			newTr.setNumeroCnh(tr.getNumeroCnh());
			newTr.setNumeroRegistroJunta(tr.getNumeroRegistroJunta());
			newTr.setNumeroRntrc(tr.getNumeroRntrc());
			newTr.setOrgaoEmissor(tr.getOrgaoEmissor());
			newTr.setSexo(tr.getSexo());
			newTr.setSituacaoRntrc(tr.getSituacaoRntrc());
			newTr.setTipoTransportador(tr.getTipoTransportador());
			newTr.setUf(tr.getUf());
			newTr.setValidadeRntrc(tr.getValidadeRntrc());
			newTr.setRowVersion(tr.getRowVersion());

			Transportador retorno = repository.save(newTr);

			tr.setAdimplenteAssociacao(retorno.isAdimplenteAssociacao());
			tr.setCapitalSocial(retorno.getCapitalSocial());
			tr.setCategoriaCnh(retorno.getCategoriaCnh());
			tr.setCodigoSituacaoRntrc(retorno.getCodigoSituacaoRntrc());
			tr.setCpfCnpj(retorno.getCpfCnpj());
			tr.setDataDeCadastro(retorno.getDataDeCadastro());
			tr.setDataDeNascimento(retorno.getDataDeNascimento());
			tr.setDescricaoSituacaoRntrc(retorno.getDescricaoSituacaoRntrc());
			tr.setEmailMovimentacaoFrota(retorno.isEmailMovimentacaoFrota());
			tr.setIdentidade(retorno.getIdentidade());
			tr.setInfoComplDescricao(retorno.getInfoComplDescricao());
			tr.setInscricaoEstadual(retorno.getInscricaoEstadual());
			tr.setInscricaoOcb(retorno.getInscricaoOcb());
			tr.setNomeCompleto(retorno.getNomeCompleto());
			tr.setNomeFantasia(retorno.getNomeFantasia());
			tr.setNomeMae(retorno.getNomeMae());
			tr.setNumeroCnh(retorno.getNumeroCnh());
			tr.setNumeroRegistroJunta(tr.getNumeroRegistroJunta());
			tr.setNumeroRntrc(retorno.getNumeroRntrc());
			tr.setOrgaoEmissor(retorno.getOrgaoEmissor());
			tr.setSexo(retorno.getSexo());
			tr.setSituacaoRntrc(retorno.getSituacaoRntrc());
			tr.setTipoTransportador(retorno.getTipoTransportador());
			tr.setUf(retorno.getUf());
			tr.setValidadeRntrc(retorno.getValidadeRntrc());
			tr.setRowVersion(retorno.getRowVersion());

			return tr;
		}
	}
}