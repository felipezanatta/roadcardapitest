package br.com.roadcard.teste.adapter.outbound.postgres.domains.notFounds;


	import org.springframework.http.HttpStatus;
	import org.springframework.web.bind.annotation.ControllerAdvice;
	import org.springframework.web.bind.annotation.ExceptionHandler;
	import org.springframework.web.bind.annotation.ResponseBody;
	import org.springframework.web.bind.annotation.ResponseStatus;

	@ControllerAdvice
	public class TransportadorNotFoundAdvice {

	  @ResponseBody
	  @ExceptionHandler(TransportadorNotFoundException.class)
	  @ResponseStatus(HttpStatus.NOT_FOUND)
	  String employeeNotFoundHandler(TransportadorNotFoundException ex) {
	    return ex.getMessage();
	  }
	}

