package br.com.roadcard.teste.adapter.outbound.postgres.domains.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.roadcard.teste.core.commons.TransportadorVO;

@Table(name="transportadores")
@Entity
public class Transportador {

	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="transportador_seq")
	@SequenceGenerator(name="transportador_seq", sequenceName="transportadores_id_seq")
	@Id
	private Long id;
	private String cpfCnpj;
	private String nomeCompleto;
	private String numeroRntrc;
	private String tipoTransportador;
	private Date dataDeCadastro;
	private Date validadeRntrc;
	private String codigoSituacaoRntrc;
	private String situacaoRntrc;
	private String descricaoSituacaoRntrc;
	private String numeroCnh;
	private String categoriaCnh;
	private String identidade;
	private String orgaoEmissor;
	private String uf;
	private Date dataDeNascimento;
	private String sexo;
	private String nomeMae;
	private String infoComplDescricao;
	private String nomeFantasia;
	private String inscricaoEstadual;
	private Double capitalSocial;
	private String numeroRegistroJunta;
	private String inscricaoOcb;
	private boolean isAdimplenteAssociacao;
	private boolean isEmailMovimentacaoFrota;
	private String rowVersion;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public String getNomeCompleto() {
		return nomeCompleto;
	}

	public void setNomeCompleto(String nomeCompleto) {
		this.nomeCompleto = nomeCompleto;
	}

	public String getNumeroRntrc() {
		return numeroRntrc;
	}

	public void setNumeroRntrc(String numeroRntrc) {
		this.numeroRntrc = numeroRntrc;
	}

	public String getTipoTransportador() {
		return tipoTransportador;
	}

	public void setTipoTransportador(String tipoTransportador) {
		this.tipoTransportador = tipoTransportador;
	}

	public Date getDataDeCadastro() {
		return dataDeCadastro;
	}

	public void setDataDeCadastro(Date dataDeCadastro) {
		this.dataDeCadastro = dataDeCadastro;
	}

	public Date getValidadeRntrc() {
		return validadeRntrc;
	}

	public void setValidadeRntrc(Date validadeRntrc) {
		this.validadeRntrc = validadeRntrc;
	}

	public String getCodigoSituacaoRntrc() {
		return codigoSituacaoRntrc;
	}

	public void setCodigoSituacaoRntrc(String codigoSituacaoRntrc) {
		this.codigoSituacaoRntrc = codigoSituacaoRntrc;
	}

	public String getSituacaoRntrc() {
		return situacaoRntrc;
	}

	public void setSituacaoRntrc(String situacaoRntrc) {
		this.situacaoRntrc = situacaoRntrc;
	}

	public String getDescricaoSituacaoRntrc() {
		return descricaoSituacaoRntrc;
	}

	public void setDescricaoSituacaoRntrc(String descricaoSituacaoRntrc) {
		this.descricaoSituacaoRntrc = descricaoSituacaoRntrc;
	}

	public String getNumeroCnh() {
		return numeroCnh;
	}

	public void setNumeroCnh(String numeroCnh) {
		this.numeroCnh = numeroCnh;
	}

	public String getCategoriaCnh() {
		return categoriaCnh;
	}

	public void setCategoriaCnh(String categoriaCnh) {
		this.categoriaCnh = categoriaCnh;
	}

	public String getIdentidade() {
		return identidade;
	}

	public void setIdentidade(String identidade) {
		this.identidade = identidade;
	}

	public String getOrgaoEmissor() {
		return orgaoEmissor;
	}

	public void setOrgaoEmissor(String orgaoEmissor) {
		this.orgaoEmissor = orgaoEmissor;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public Date getDataDeNascimento() {
		return dataDeNascimento;
	}

	public void setDataDeNascimento(Date dataDeNascimento) {
		this.dataDeNascimento = dataDeNascimento;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getNomeMae() {
		return nomeMae;
	}

	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}

	public String getInfoComplDescricao() {
		return infoComplDescricao;
	}

	public void setInfoComplDescricao(String infoComplDescricao) {
		this.infoComplDescricao = infoComplDescricao;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public String getInscricaoEstadual() {
		return inscricaoEstadual;
	}

	public void setInscricaoEstadual(String inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}

	public Double getCapitalSocial() {
		return capitalSocial;
	}

	public void setCapitalSocial(Double capitalSocial) {
		this.capitalSocial = capitalSocial;
	}

	public String getNumeroRegistroJunta() {
		return numeroRegistroJunta;
	}

	public void setNumeroRegistroJunta(String numeroRegistroJunta) {
		this.numeroRegistroJunta = numeroRegistroJunta;
	}

	public String getInscricaoOcb() {
		return inscricaoOcb;
	}

	public void setInscricaoOcb(String inscricaoOcb) {
		this.inscricaoOcb = inscricaoOcb;
	}

	public boolean isAdimplenteAssociacao() {
		return isAdimplenteAssociacao;
	}

	public void setAdimplenteAssociacao(boolean isAdimplenteAssociacao) {
		this.isAdimplenteAssociacao = isAdimplenteAssociacao;
	}

	public boolean isEmailMovimentacaoFrota() {
		return isEmailMovimentacaoFrota;
	}

	public void setEmailMovimentacaoFrota(boolean isEmailMovimentacaoFrota) {
		this.isEmailMovimentacaoFrota = isEmailMovimentacaoFrota;
	}

	public String getRowVersion() {
		return rowVersion;
	}

	public void setRowVersion(String rowVersion) {
		this.rowVersion = rowVersion;
	}
	
	public static TransportadorVO buildVOFromEntity(Transportador enti) {
		
		TransportadorVO transportador = new TransportadorVO();
		transportador.setId(enti.getId());
		transportador.setCpfCnpj(enti.getCpfCnpj());
		transportador.setNomeCompleto(enti.getNomeCompleto());
		transportador.setNumeroRntrc(enti.getNumeroRntrc());
		transportador.setTipoTransportador(enti.getTipoTransportador());
		transportador.setDataDeCadastro(enti.getDataDeCadastro());
		transportador.setValidadeRntrc(enti.getValidadeRntrc());
		transportador.setCodigoSituacaoRntrc(enti.getCodigoSituacaoRntrc());
		transportador.setSituacaoRntrc(enti.getSituacaoRntrc());
		transportador.setDescricaoSituacaoRntrc(enti.getDescricaoSituacaoRntrc());
		transportador.setNumeroCnh(enti.getNumeroCnh());
		transportador.setCategoriaCnh(enti.getCategoriaCnh());
		transportador.setIdentidade(enti.getIdentidade());
		transportador.setOrgaoEmissor(enti.getOrgaoEmissor());
		transportador.setUf(enti.getUf());
		transportador.setDataDeNascimento(enti.getDataDeNascimento());
		transportador.setSexo(enti.getSexo());
		transportador.setNomeMae(enti.getNomeMae());
		transportador.setInfoComplDescricao(enti.getInfoComplDescricao());
		transportador.setNomeFantasia(enti.getNomeFantasia());
		transportador.setInscricaoEstadual(enti.getInscricaoEstadual());
		transportador.setCapitalSocial(enti.getCapitalSocial());
		transportador.setNumeroRegistroJunta(enti.getNumeroRegistroJunta());
		transportador.setInscricaoEstadual(enti.getInscricaoOcb());
		transportador.setAdimplenteAssociacao(enti.isAdimplenteAssociacao());
		transportador.setEmailMovimentacaoFrota(enti.isEmailMovimentacaoFrota());
		
		return transportador;
	}
	
	public static Transportador buildEntityFromVO(TransportadorVO vo) {
		
		Transportador transportador = new Transportador();
		transportador.setCpfCnpj(vo.getCpfCnpj());
		transportador.setNomeCompleto(vo.getNomeCompleto());
		transportador.setNumeroRntrc(vo.getNumeroRntrc());
		transportador.setTipoTransportador(vo.getTipoTransportador());
		transportador.setDataDeCadastro(vo.getDataDeCadastro());
		transportador.setValidadeRntrc(vo.getValidadeRntrc());
		transportador.setCodigoSituacaoRntrc(vo.getCodigoSituacaoRntrc());
		transportador.setSituacaoRntrc(vo.getSituacaoRntrc());
		transportador.setDescricaoSituacaoRntrc(vo.getDescricaoSituacaoRntrc());
		transportador.setNumeroCnh(vo.getNumeroCnh());
		transportador.setCategoriaCnh(vo.getCategoriaCnh());
		transportador.setIdentidade(vo.getIdentidade());
		transportador.setOrgaoEmissor(vo.getOrgaoEmissor());
		transportador.setUf(vo.getUf());
		transportador.setDataDeNascimento(vo.getDataDeNascimento());
		transportador.setSexo(vo.getSexo());
		transportador.setNomeMae(vo.getNomeMae());
		transportador.setInfoComplDescricao(vo.getInfoComplDescricao());
		transportador.setNomeFantasia(vo.getNomeFantasia());
		transportador.setInscricaoEstadual(vo.getInscricaoEstadual());
		transportador.setCapitalSocial(vo.getCapitalSocial());
		transportador.setNumeroRegistroJunta(vo.getNumeroRegistroJunta());
		transportador.setInscricaoEstadual(vo.getInscricaoOcb());
		transportador.setAdimplenteAssociacao(vo.isAdimplenteAssociacao());
		transportador.setEmailMovimentacaoFrota(vo.isEmailMovimentacaoFrota());
		return transportador;
	}

}
