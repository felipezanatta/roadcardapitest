package br.com.roadcard.teste.adapter.outbound.postgres.domains.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.roadcard.teste.adapter.outbound.postgres.domains.entity.Transportador;

public interface TransRepository extends JpaRepository<Transportador, Long> {

}
