package br.com.roadcard.teste.adapter.inbound.rest.controller;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.roadcard.teste.core.commons.TransportadorVO;
import br.com.roadcard.teste.core.port.inbound.TransportadorPortInbound;

@RestController
public class TransportadorController {

	@Autowired
	TransportadorPortInbound coreHandler;

	@GetMapping("/transportadores")
	public List<TransportadorVO> all() {

		List<TransportadorVO> transportadores = coreHandler.getAll();

		return transportadores;
	}

	@RequestMapping(value = "/transportador/{id}", method = RequestMethod.GET)
	public TransportadorVO getById(@PathVariable(value = "id") long id) {

		TransportadorVO transportador = coreHandler.findById(id);

		return transportador;

	}

	@RequestMapping(value = "/transportador", method = RequestMethod.POST)
	public TransportadorVO create(@RequestBody TransportadorVO transportador) {

		TransportadorVO transp = coreHandler.persist(transportador);

		return transp;
	}

	@RequestMapping(value = "/transportador/{id}", method = RequestMethod.PUT)
	public TransportadorVO upDate(@PathParam (value = "id") Long id, TransportadorVO tr) {

		TransportadorVO transportador = coreHandler.upDate(tr);

		return transportador;

	}
}
