package br.com.roadcard.teste.adapter.outbound.postgres.domains.notFounds;

@SuppressWarnings("serial")
public class TransportadorNotFoundException extends RuntimeException {

	public TransportadorNotFoundException(Long id) {
		super("Não foi possível encontrar o Transportador = " + id);
	}
}
