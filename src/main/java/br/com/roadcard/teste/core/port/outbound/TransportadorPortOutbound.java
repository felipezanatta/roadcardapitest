package br.com.roadcard.teste.core.port.outbound;

import java.util.List;

import br.com.roadcard.teste.core.commons.TransportadorVO;

public interface TransportadorPortOutbound {

	public List<TransportadorVO> getAll();

	public TransportadorVO findById(Long id);

	public TransportadorVO persist(TransportadorVO tr);
	
	public TransportadorVO upDate(TransportadorVO tr);

	TransportadorVO upDate(Long id, TransportadorVO tr);
}
