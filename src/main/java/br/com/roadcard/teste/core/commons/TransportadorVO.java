package br.com.roadcard.teste.core.commons;

import java.util.Date;

public class TransportadorVO {
	
	private Long id;
	private String cpfCnpj;
	private String nomeCompleto;
	private String numeroRntrc;
	private String tipoTransportador;
	private Date dataDeCadastro;
	private Date validadeRntrc;
	private String codigoSituacaoRntrc;
	private String situacaoRntrc;
	private String descricaoSituacaoRntrc;
	private String numeroCnh;
	private String categoriaCnh;
	private String identidade;
	private String orgaoEmissor;
	private String uf;
	private Date dataDeNascimento;
	private String sexo;
	private String nomeMae;
	private String infoComplDescricao;
	private String nomeFantasia;
	private String inscricaoEstadual;
	private Double capitalSocial;
	private String numeroRegistroJunta;
	private String inscricaoOcb;
	private boolean isAdimplenteAssociacao;
	private boolean isEmailMovimentacaoFrota;
	private String rowVersion;
	
	public String getRowVersion() {
		return rowVersion;
	}
	public void setRowVersion(String rowVersion) {
		this.rowVersion = rowVersion;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCpfCnpj() {
		return cpfCnpj;
	}
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}
	public String getNomeCompleto() {
		return nomeCompleto;
	}
	public void setNomeCompleto(String nomeCompleto) {
		this.nomeCompleto = nomeCompleto;
	}
	public String getNumeroRntrc() {
		return numeroRntrc;
	}
	public void setNumeroRntrc(String numeroRntrc) {
		this.numeroRntrc = numeroRntrc;
	}
	public String getTipoTransportador() {
		return tipoTransportador;
	}
	public void setTipoTransportador(String tipoTransportador) {
		this.tipoTransportador = tipoTransportador;
	}
	public Date getDataDeCadastro() {
		return dataDeCadastro;
	}
	public void setDataDeCadastro(Date dataDeCadastro) {
		this.dataDeCadastro = dataDeCadastro;
	}
	public Date getValidadeRntrc() {
		return validadeRntrc;
	}
	public void setValidadeRntrc(Date validadeRntrc) {
		this.validadeRntrc = validadeRntrc;
	}
	public String getCodigoSituacaoRntrc() {
		return codigoSituacaoRntrc;
	}
	public void setCodigoSituacaoRntrc(String codigoSituacaoRntrc) {
		this.codigoSituacaoRntrc = codigoSituacaoRntrc;
	}
	public String getSituacaoRntrc() {
		return situacaoRntrc;
	}
	public void setSituacaoRntrc(String situacaoRntrc) {
		this.situacaoRntrc = situacaoRntrc;
	}
	public String getDescricaoSituacaoRntrc() {
		return descricaoSituacaoRntrc;
	}
	public void setDescricaoSituacaoRntrc(String descricaoSituacaoRntrc) {
		this.descricaoSituacaoRntrc = descricaoSituacaoRntrc;
	}
	public String getNumeroCnh() {
		return numeroCnh;
	}
	public void setNumeroCnh(String numeroCnh) {
		this.numeroCnh = numeroCnh;
	}
	public String getCategoriaCnh() {
		return categoriaCnh;
	}
	public void setCategoriaCnh(String categoriaCnh) {
		this.categoriaCnh = categoriaCnh;
	}
	public String getIdentidade() {
		return identidade;
	}
	public void setIdentidade(String identidade) {
		this.identidade = identidade;
	}
	public String getOrgaoEmissor() {
		return orgaoEmissor;
	}
	public void setOrgaoEmissor(String orgaoEmissor) {
		this.orgaoEmissor = orgaoEmissor;
	}
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public Date getDataDeNascimento() {
		return dataDeNascimento;
	}
	public void setDataDeNascimento(Date dataDeNascimento) {
		this.dataDeNascimento = dataDeNascimento;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getNomeMae() {
		return nomeMae;
	}
	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}
	public String getInfoComplDescricao() {
		return infoComplDescricao;
	}
	public void setInfoComplDescricao(String infoComplDescricao) {
		this.infoComplDescricao = infoComplDescricao;
	}
	public String getNomeFantasia() {
		return nomeFantasia;
	}
	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}
	public String getInscricaoEstadual() {
		return inscricaoEstadual;
	}
	public void setInscricaoEstadual(String inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}
	public Double getCapitalSocial() {
		return capitalSocial;
	}
	public void setCapitalSocial(Double capitalSocial) {
		this.capitalSocial = capitalSocial;
	}
	public String getNumeroRegistroJunta() {
		return numeroRegistroJunta;
	}
	public void setNumeroRegistroJunta(String numeroRegistroJunta) {
		this.numeroRegistroJunta = numeroRegistroJunta;
	}
	public String getInscricaoOcb() {
		return inscricaoOcb;
	}
	public void setInscricaoOcb(String inscricaoOcb) {
		this.inscricaoOcb = inscricaoOcb;
	}
	public boolean isAdimplenteAssociacao() {
		return isAdimplenteAssociacao;
	}
	public void setAdimplenteAssociacao(boolean isAdimplenteAssociacao) {
		this.isAdimplenteAssociacao = isAdimplenteAssociacao;
	}
	public boolean isEmailMovimentacaoFrota() {
		return isEmailMovimentacaoFrota;
	}
	public void setEmailMovimentacaoFrota(boolean isEmailMovimentacaoFrota) {
		this.isEmailMovimentacaoFrota = isEmailMovimentacaoFrota;
	}
	
	

}
