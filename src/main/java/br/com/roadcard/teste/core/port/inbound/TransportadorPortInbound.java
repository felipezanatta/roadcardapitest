package br.com.roadcard.teste.core.port.inbound;

import java.util.List;

import br.com.roadcard.teste.core.commons.TransportadorVO;

public interface TransportadorPortInbound {

	public List<TransportadorVO> getAll();

	public TransportadorVO findById(Long id);
	
	public TransportadorVO persist(TransportadorVO tr);
	
	public TransportadorVO upDate(TransportadorVO tr);

}
