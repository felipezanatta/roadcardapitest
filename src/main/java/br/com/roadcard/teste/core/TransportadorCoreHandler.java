package br.com.roadcard.teste.core;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.roadcard.teste.core.commons.TransportadorVO;
import br.com.roadcard.teste.core.port.inbound.TransportadorPortInbound;
import br.com.roadcard.teste.core.port.outbound.TransportadorPortOutbound;

@Service
public class TransportadorCoreHandler implements TransportadorPortInbound {

	@Autowired
	TransportadorPortOutbound portOutbound;

	@Override
	public List<TransportadorVO> getAll() {
		return portOutbound.getAll();
	}

	@Override
	public TransportadorVO findById(Long id) {
		return portOutbound.findById(id);
	}

	public TransportadorVO persist(TransportadorVO tr) {
		return portOutbound.persist(tr);
	}

	public TransportadorVO upDate(TransportadorVO tr) {
		return portOutbound.upDate(tr);
	}
}